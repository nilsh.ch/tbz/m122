# M122 Hindermann Bash Script

Das Bash-Skript von Nils Hindermann lädt per FTP jeweils eine Datei herunter und
zählt die Anzahl der richtigen und falschen Datensätze und
schickt zwei einzelne Dateien wieder zurück (eine für Richtig / eine für Falsch)

Schauen Sie sich das [Setup](./setup.md) an

[![UML-Aktivitätsdiagramm](./diagram.png)](./diagram.png)

Text-Beschreibung, was das Skript macht:
```
Start
|
| Read config
| Increase build number
| Declare version with run
| Set script variables
|
V
[Script Execution]
|
| Connect to FTP Server
| Download File
| Process File
| Upload Incorrect File
| Upload Correct File
| Mail Log
| Clean Up
|
V
End
```