# M122 Hindermann Bash Script Setup

Schritte, die notwendig sind, um das Skript ausführen zu können

1. script/script.sh und script/config.cfg in einen Ordner kopieren
1. Das Skript muss ausführbar gemacht werden mit `chmod +x script.sh`
1. *(Optional)* cronjob einrichten (mit `cronjob -e`)
   - Beispiel: `0 */2 * * * /home/ubuntu/script.sh`
     So wird das Skript jede zweite Stunde ausgeführt.
   - mit `MAILTO="nils.hindermann@edu.tbz.ch"` kann man sich eine E-Mail schicken lassen
1. Das Skript kann auch manuell ausgeführt werden, dann wird nur eine E-Mail verschickt.
