#!/bin/bash

# Read config
. ./config.cfg
. ./version

# Increase build number
((build+=1))
echo "build=$build" > ./version

# Declare version with run
declare -r VERSION="v2023.7.14 (Run #$build)"

# Script variables
errors=0
data_correct=0
data_incorrect=0
file_incorrect="${build}_incorrect.txt"
file_correct="${build}_correct.txt"

# Write header
echo "M122 $VERSION"
echo "=================="
echo "by Nils Hindermann"
echo

# Logfile separation
echo > $CURRENT_LOG_FILE
echo >> $FULL_LOG_FILE
echo "======= [ LOG BEGIN ] =======" >> $FULL_LOG_FILE

# Functions
# Logging
function log() {
    now=`date +%F_%T`
    msg="($now) $1"
    echo $msg
    echo $msg >> $CURRENT_LOG_FILE
    echo $msg >> $FULL_LOG_FILE
}

# Function to upload a file via FTP
# First parameter is the local file name
# Second parameter is the remote file name
function upload() {
    {
        log "Connecting to FTP server..."
        ftp -n $FTP_SERVER <<END_SCRIPT
quote USER $FTP_USERNAME
quote PASS $FTP_PASSWORD
binary
put $1 $2
quit
END_SCRIPT
        log "Processed file uploaded. ($1)"
    } || ((errors+=1))
}

# Log a message
log "Script executing now!"

# Start and log Script
echo >> $FULL_LOG_FILE
echo "=== RUNNING SCRIPT ===" >> $FULL_LOG_FILE
log "Version: $VERSION"
log "Current Date: `date "+%Y%m%d-%H%M%S"`"
log "Using FTP Server: $FTP_SERVER"
log "Using FTP User: $FTP_USERNAME"

# FTP connection and file download
mkdir "$LOCAL_DATA_FOLDER"
log "Connecting to FTP server..."
ftp -n $FTP_SERVER <<END_SCRIPT
quote USER $FTP_USERNAME
quote PASS $FTP_PASSWORD
binary
get $REMOTE_DATA_FOLDER/$REMOTE_FILE $LOCAL_DATA_FOLDER/$LOCAL_FILE
quit
END_SCRIPT
log "Data file downloaded."

# Process file and move to separate folder
log "Processing data file..."
while IFS= read -r line; do
    # Check if line does not contain incorrect data
    if [[ $line == *"-1"* ]]; then
        ((data_incorrect+=1))
        echo "$line" >> $file_incorrect
    else
        ((data_correct+=1))
        echo "$line" >> $file_correct
    fi
done < "$LOCAL_DATA_FOLDER/$LOCAL_FILE"
log "Data file processed."
log "Correct/Incorrect data: $data_correct / $data_incorrect"

# Upload processed files via FTP
upload $file_incorrect "$REMOTE_DATA_FOLDER/processed/$file_incorrect"
upload $file_correct "$REMOTE_DATA_FOLDER/processed/$file_correct"

# Mail current log
cat $CURRENT_LOG_FILE | mail -s "$MAIL_SUBJECT" "$MAIL_RECIPIENT" -A $CURRENT_LOG_FILE

# Clean up (remove local files)
rm "$CURRENT_LOG_FILE"
rm "$LOCAL_DATA_FOLDER/$LOCAL_FILE"
rm "$file_incorrect"
rm "$file_correct"
rmdir "$LOCAL_DATA_FOLDER"
log "Cleaned up."

# End of script
if (($errors > 0)); then
    log "Script was completed with $errors errors"
else
    log "Script was completed without errors"
fi
